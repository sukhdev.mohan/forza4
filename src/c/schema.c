#define nRighe 6
#define nColonne 7

#include <stdio.h>
#include <locale.h>

enum colori
{
    nero,
    rosso,
    giallo,
    verde,
    blu 
};

int pezzi[] = {0x250C, 0x2500, 0x252C, 0x2510, 0x2502, 0x251C, 0x253C, 0x2524, 0x2514, 0x2534, 0x2518};

void stampa(int tabellone[][nColonne]) {
	setlocale(LC_CTYPE, "");
	printf("\n\n");
	int i = 0,
        j = 0;
        
	printf("%lc%lc%lc%lc%lc%lc%lc%lc%lc%lc%lc%lc%lc%lc%lc\n",
        pezzi[0], pezzi[1], pezzi[2], pezzi[1], pezzi[2], pezzi[1],
        pezzi[2], pezzi[1], pezzi[2], pezzi[1], pezzi[2], pezzi[1],
        pezzi[2], pezzi[1], pezzi[3]);

    // Inizio stampa tabellone
	for (i = 0; i < nRighe; i++) {
		for ( j = 0 ; j < nColonne ; j++ ) {
			switch (tabellone[i][j]) {
                case nero:
                    printf("%lc ", pezzi[4]);
                    break;
                case rosso:
                    printf("%lc\e[31m%lc\e[0m", pezzi[4], 0x2B24);
                    break;
                case giallo:
                    printf("%lc\e[33m%lc\e[0m", pezzi[4], 0x2B24);
                    break;
                case verde:
                    printf("%lc\e[32m%lc\e[0m", pezzi[4], 0x2B24);
                    break;
                case blu:
                    printf("%lc\e[34m%lc\e[0m", pezzi[4], 0x2B24);
                    break;
                default:
                    printf("%lce", pezzi[4]);
                    break;
            }
		}

		if (i == 5) {
			printf("%lc\n%lc%lc%lc%lc%lc%lc%lc%lc%lc%lc%lc%lc%lc%lc%lc\n",
                pezzi[4], pezzi[8], pezzi[1], pezzi[9], pezzi[1], pezzi[9],
                pezzi[1], pezzi[9], pezzi[1], pezzi[9], pezzi[1], pezzi[9], 
                pezzi[1], pezzi[9], pezzi[1], pezzi[10]);
		} else {
			printf("%lc\n%lc%lc%lc%lc%lc%lc%lc%lc%lc%lc%lc%lc%lc%lc%lc\n",
                pezzi[4], pezzi[5], pezzi[1], pezzi[6], pezzi[1], pezzi[6], 
                pezzi[1], pezzi[6], pezzi[1], pezzi[6], pezzi[1], pezzi[6],
                pezzi[1], pezzi[6], pezzi[1], pezzi[7]);
		}
	}
}

/*
*    Il metodo restituisce 0 se non riesce ad inserire la pedina in quanto la colonna
*    è piena. Se è stato possibile inserire la pedina, restituisce il numero di riga
*    in cui è stata inserita. Restituisce tra 1 e numeroRighe.
*/
int addPedina (int giocatore, int tabellone[][nColonne], int colonna) {
    int cellaLibera = -1,
        i = nRighe;
    for (i = nRighe-1; i >= 0; i--) {
        if (tabellone[i][colonna] == 0) {
            cellaLibera=i;
            break;
        }
    }

    if (cellaLibera != -1) {
        tabellone[cellaLibera][colonna] = giocatore;
        stampa(tabellone);
        return cellaLibera + 1;
    }

    return 0;
}