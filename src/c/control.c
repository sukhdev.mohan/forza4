// Aggiungere:
  #include <stdio.h>
  #include <stdbool.h>
  #include "../include/schema.h"
  #define ALTEZZA 6
  #define LARGHEZZA 7

//Prototipi
bool gettone_giocatore(int tabellone[ALTEZZA][LARGHEZZA], int x, int y, int giocatore);
bool victory(int giocatore, int altezza, int larghezza, int tabellone[ALTEZZA][LARGHEZZA]);

bool gettone_giocatore(int tabellone[ALTEZZA][LARGHEZZA], int x, int y, int giocatore){
  if(tabellone[x][y] == giocatore) return true;
  else return false;
}

bool victory(int giocatore, int altezza, int larghezza, int tabellone[ALTEZZA][LARGHEZZA]){
  for(int i=0; i<altezza; i++){
    for(int j=0; j<larghezza; j++){
      if(tabellone[i][j] == giocatore){
        //Controllo dir. alto sx
        if((i+3 < altezza)&&(j-3 > 0)){
          if(gettone_giocatore(tabellone, i+3, j-3, giocatore)){
            if(gettone_giocatore(tabellone, i+2, j-2, giocatore)){
              if(gettone_giocatore(tabellone, i+1, j-1, giocatore)){
                return true;
              }
            }
          }
        }
        //Controllo dir. alto
        if(i+3 < altezza){
          if(gettone_giocatore(tabellone, i+3, j, giocatore)){
            if(gettone_giocatore(tabellone, i+2, j, giocatore)){
              if(gettone_giocatore(tabellone, i+1, j, giocatore)){
                return true;
              }
            }
          }
        }
        //Controllo dir. alto dx
        if((i+3 < altezza)&&(j+3 < larghezza)){
          if(gettone_giocatore(tabellone, i+3, j+3, giocatore)){
            if(gettone_giocatore(tabellone, i+2, j+2, giocatore)){
              if(gettone_giocatore(tabellone, i+1, j+1, giocatore)){
                return true;
              }
            }
          }
        }
        //Controllo dir. sx
        if(j-3 > 0){
          if(gettone_giocatore(tabellone, i, j-3, giocatore)){
            if(gettone_giocatore(tabellone, i, j-2, giocatore)){
              if(gettone_giocatore(tabellone, i, j-1, giocatore)){
                return true;
              }
            }
          }
        }
        //Controllo dir. dx
        if(j+3 < larghezza){
          if(gettone_giocatore(tabellone, i, j+3, giocatore)){
            if(gettone_giocatore(tabellone, i, j+2, giocatore)){
              if(gettone_giocatore(tabellone, i, j+1, giocatore)){
                return true;
              }
            }
          }
        }
        //Controllo dir. basso sx
        if((i-3 > 0)&&(j-3 > 0)){
          if(gettone_giocatore(tabellone, i-3, j-3, giocatore)){
            if(gettone_giocatore(tabellone, i-2, j-2, giocatore)){
              if(gettone_giocatore(tabellone, i-1, j-1, giocatore)){
                return true;
              }
            }
          }
        }
        //Controllo dir. basso
        if(i-3 > 0){
          if(gettone_giocatore(tabellone, i-3, j, giocatore)){
            if(gettone_giocatore(tabellone, i-2, j, giocatore)){
              if(gettone_giocatore(tabellone, i-1, j, giocatore)){
                return true;
              }
            }
          }
        }
        //Controllo dir. basso dx
        if((i-3 > 0)&&(j+3 < larghezza)){
          if(gettone_giocatore(tabellone, i-3, j+3, giocatore)){
            if(gettone_giocatore(tabellone, i-2, j+2, giocatore)){
              if(gettone_giocatore(tabellone, i-1, j+1, giocatore)){
                return true;
              }
            }
          }
        }
      }
    }
  }
  return false;
}

//main per testare il funzionamento della funzione
/* 
int main()
{
  bool vittoria=false;
  int tab[ALTEZZA][LARGHEZZA], i=0, j=0, giocatore=0, colonna=0;
  for (i=0;i<ALTEZZA;i++)
  {
    for (j=0;j<LARGHEZZA;j++)
    {
      tab[i][j]=0;
    }
  }
  stampa(*tab);
  while (!vittoria)
  {
    printf("Giocatore: ");
    scanf("%d", &giocatore);
    printf("Colonna: ");
    scanf("%d", &colonna);
    addPedina(giocatore,*tab, colonna);
    vittoria=victory(1,ALTEZZA,LARGHEZZA,tab);
    if (vittoria)
    {
      printf("VITTORIA giocatore %d\n", giocatore);
    }
    else
    {
      printf("Non abbiamo ancora una vittoria del giocatore %d\n", giocatore);
    }
  }
} */