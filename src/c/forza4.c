#define LARGHEZZA 7
#define ALTEZZA   6

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include "../include/schema.h"
#include "../include/control.h"

enum colori{
    nero,
    rosso,
    giallo,
    verde,
    blu 
};

int getNumeroGiocatori();
void inizializzaGiocatori(int numeroGiocatori, char *giocatori[]);
int gestioneTurni(int numeroGiocatori, int indiceCiclo, char *giocatori[], int tabellone[][LARGHEZZA]);
int turnoAI (char *giocatori[], int tabellone[][LARGHEZZA]);

int main(int argc, char *argv[])
{
    // Il tabellone del gioco
    int tabellone[ALTEZZA][LARGHEZZA];
    // Il numero di giocatori
    int numeroGiocatori;
    // variabili di utilità
    int i, j;
    // srand
    time_t t;
    srand((unsigned) time(&t));
    // I giocatori effettivi
    numeroGiocatori = getNumeroGiocatori();

    if (!numeroGiocatori)  return 0;

    char *giocatori[numeroGiocatori];

    // inserimento nome giocatori
    inizializzaGiocatori(numeroGiocatori, giocatori);

    // Inserisco l'AI fra i giocatori
    if (numeroGiocatori == 1) {
        giocatori[1] = (char *) malloc(11 * sizeof(char));
        giocatori[1] = "AI";
    }

    // Inizializza il tabellone
    for (i = 0; i < ALTEZZA; i++) 
        for (j = 0; j < LARGHEZZA; j++)
            tabellone[i][j] = 0;

    // Stampa il nome dei partecipanti 
    for (i = 0; i < numeroGiocatori; i++) {
        printf("Giocatore %d: %s\n", i+1, giocatori[i]);
    }
    if (numeroGiocatori == 1) printf("Giocatore 2: %s\n", giocatori[1]);

    // Stampa il tabellone vuoto
    stampa(tabellone);

    // Inizia il gioco
    for (i = 0; i < 42; i++) {
        if (gestioneTurni(numeroGiocatori, i, giocatori, tabellone))
            return 1;
    }

    printf ("Nessuno ha vinto :( ...");
    return 0;
}

int getNumeroGiocatori() {
    int numero;
    while (1) {
        // Numero di giocatori, min 2 max 4
        printf("Inserire il numero giocatori: ");
        scanf("%d", &numero);
        while((getchar()) != '\n');

        if (numero >= 1 && numero <= 4) {
            return numero;
            break;
        }
        printf ("Errore: numero di giocatori invalido. Inserire un numero tra 2 e 4.\n");
    }

    return 0;
}

void inizializzaGiocatori(int numeroGiocatori, char *giocatori[]) {
    // Per ogni giocatore inserire il nome 
    int i;
    for (i = 0; i < numeroGiocatori; i++) {
        
        // Alloca lo spazio necessario per il nome massimo 10 caratteri
        giocatori[i] = (char*) malloc(11 * sizeof(char));
        printf("Inserire il nome del %d giocatore (massimo 10 caratteri): ", i + 1);
        scanf("%s", giocatori[i]);
        while((getchar()) != '\n');   
    }
}

int gestioneTurni(int numeroGiocatori, int indiceCiclo, char *giocatori[], int tabellone[][LARGHEZZA]) {
    int i, colonna, risultato;

    // Calcola turno
    i = indiceCiclo % numeroGiocatori;

    printf("E' il turno del giocatore %s, inserisci numero della colonna (tra 1 e 7) in cui inserire il gettone: ", giocatori[i]);
    while(1){
        scanf("%d", &colonna);
        while((getchar()) != '\n');

        if(colonna >= 1 && colonna <= 7 ){
            break;
        }
        printf("Colonna non valida! Inserisci una colonna tra 1 e 7: ");
    }

    risultato = addPedina(i + 1, tabellone, colonna - 1);

    while (!risultato) {
        printf("La colonna selezionata è piena. Scegli un'altra colonna: ");
        scanf("%d", &colonna);
        while((getchar()) != '\n');

        risultato = addPedina(i + 1, tabellone, colonna - 1);
    }

    // Controlla se il giocatore corrente ha vinto
    if (victory(i + 1, ALTEZZA, LARGHEZZA, tabellone)) {
        printf("Il Giocatore %s ha vinto!\n", giocatori[i]);
        return true;
    }

    // Tocca al AI
    if (numeroGiocatori == 1) {
        return turnoAI(giocatori, tabellone);
    }

    return false;
}

int turnoAI (char *giocatori[], int tabellone[][LARGHEZZA]) {
    int i, colonna, risultato;
    colonna = rand() % 7;
    printf("E' il turno del giocatore %s, inserisci numero della colonna (tra 1 e 7) in cui inserire il gettone: %d\n", giocatori[1], colonna+1);
    risultato = addPedina(2, tabellone, colonna);

    while (!risultato) {
        colonna = rand() % 8;
        printf("La colonna selezionata è piena. Scegli un'altra colonna: %d\n", colonna);

        risultato = addPedina(2, tabellone, colonna - 1);
    }

    if (victory(2, ALTEZZA, LARGHEZZA, tabellone)) {
        printf("Il Giocatore %s ha vinto!\n", giocatori[1]);
        return true;
    }

    return false;
}