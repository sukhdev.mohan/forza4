# forza4

## DEVELOPERS

* Gualandris Gianluca
* Mohan Sukhdev
* Piccoli Paolo

### Project for the Skull of Summer 2018 - Student Branch IEEE Brescia

This is a `C` version of the classic game [Forza 4](https://it.wikipedia.org/wiki/Forza_quattro)

#### ¬Features
You can play in solo mode with a [stoopid](https://www.urbandictionary.com/define.php?term=stoopid) AI or in local 2-4 multiplayer mode

*Cumming soon*: Server-Client **WIP**

# Play at your own risk